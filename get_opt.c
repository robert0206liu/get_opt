#include <stdio.h>
#include <stdlib.h> // exit()
#include <unistd.h> // getopt()

void DisplayUsage()
{
    printf("get_opt [OPTIONS]\n");
    printf("\t-a\n");
    printf("\t-b <ARGUMENT>\n");
    printf("\t-c <ARGUMENT1> <ARGUMENT2>\n");
    printf("\t-d <OPTIONAL ARGUMENT>\n");
    
    exit(0);
}

int main(int argc, char *argv[])
{
    int opt = 0;
    int arg_required = 0;
    int arg_counter = 0;
    
    printf("argc = %d\n", argc);
    
    while(1)
    {
        printf("Process argv index: %d\n", optind);
        
        opt = getopt(argc, argv, "ab:c:d::h");
        
        if (opt == -1)
        {
            break;
        }
        
        if (opt != '?') {
            printf("Option: -%c\n", opt);
        }
        
        switch(opt)
        {
            case 'a':
                // optarg = ?
                break;
                
            case 'b':
                if(optarg != NULL)
                {
                    printf("Argument: %s\n", optarg);
                }
                else
                {
                    printf("Missing argument for opt -b\n");
                    DisplayUsage();
                }
                break;
            
            case 'c':
                arg_required = 2;
                // After getopt(), optind will point to the next argv[] of optarg
                // optarg = argv[optind-1]
                // So, we decrease optind to point to the first argv[] of current opt
                optind--;
                
                for(arg_counter=0; arg_counter<arg_required; arg_counter++)
                {
                    if(optind >= argc)
                    {
                        printf("Missing argument%d for opt -c\n", arg_counter+1);
                        DisplayUsage();
                    }
                    
                    if(*argv[optind] == '-')
                    {
                        printf("Missing argument%d for opt -c\n", arg_counter+1);
                        DisplayUsage();
                    }

                    if(argv[optind] != NULL)
                    {
                        printf("Argument%d: %s\n", arg_counter+1, argv[optind]);
                    }
                    else
                    {
                        printf("Missing argument%d for opt -c\n", arg_counter+1);
                        DisplayUsage();
                    }
                    optind++;
                }
                break;
                
            case 'd':
                if(optarg != NULL)
                {
                    printf("Argument: %s\n", optarg);
                }
                else
                {
                    printf("Argument: None\n");
                }
                break;
                
            case 'h':
            default:
                DisplayUsage();
                break;
        }
        printf("\n");
    }
}
