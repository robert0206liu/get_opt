all: get_opt.c
	gcc get_opt.c -o get_opt

debug: get_opt.c
	gcc -g get_opt.c -o get_opt

clean:
	rm get_opt
